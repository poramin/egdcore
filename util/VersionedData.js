var base58 = require('../lib/Base58').base58Check;
var util = require('util');
var EncodedData = require('./EncodedData');


function VersionedData(version, payload, namespace) {
  console.log("Init VersionedData");
  VersionedData.super_.call(this, version, payload, namespace);
  if (typeof version != 'number') {
    return;
  };
  if (typeof namespace !== "undefined" && typeof namespace != 'number') {
    return;
  };
  this.encoding('binary');
  if (typeof namespace !== "undefined"){
        console.log("Namespace is not undefined");
  	this.data = new Buffer(payload.length + 2);
	console.log(namespace);
        this.namespace(namespace);
	this._namespace = namespace;
  }else{
	console.log("Namespace is undefined");
 	this.data = new Buffer(payload.length + 1); 
	this._namespace = undefined;
  }
  this.version(version);
  this.payload(payload);
};

util.inherits(VersionedData, EncodedData);
EncodedData.applyEncodingsTo(VersionedData);

// get or set the version data (the first byte of the address)
VersionedData.prototype.namespace = function(num) {
  	if (num || (num === 0)) {
    	   this.doAsBinary(function() {
      		this.data.writeUInt8(num, 0);
    	   });
    	   return num;
  	}
  	return this.as('binary').readUInt8(0);
};

// get or set the version data (the second byte of the address)
VersionedData.prototype.version = function(num) {
  if (this._namespace){
        if (num || (num === 0)) {
           this.doAsBinary(function() {
                this.data.writeUInt8(num, 1);
           });
           return num;
        }
        return this.as('binary').readUInt8(1);
  }else{
       if (num || (num === 0)) {
           this.doAsBinary(function() {
                this.data.writeUInt8(num, 0);
           });
           return num;
        }
        return this.as('binary').readUInt8(0);

 }

};

// get or set the payload data (as a Buffer object)
VersionedData.prototype.payload = function(data) {
  if (this._namespace){
        if (data) {
                this.doAsBinary(function() {
                        data.copy(this.data, 2);
                });
                return data;
        }
        return this.as('binary').slice(1);
  }else{
        if (data) {
                this.doAsBinary(function() {
                        data.copy(this.data, 1);
                });
                return data;
        }
        return this.as('binary').slice(1);
  }
};

module.exports = VersionedData;
