var Put = require('bufferput');
var buffertools = require('buffertools');
var hex = function(hex) {
  return new Buffer(hex, 'hex');
};

exports.livenet = {
  name: 'livenet',
  magic: hex('70352205'),
  addressVersion: 0x21,
  privKeyVersion: 161,
  P2SHVersion: 85,
  hkeyPublicVersion: 0x0488b21e,
  hkeyPrivateVersion: 0x0488ade4,
  genesisBlock: {
    hash: hex('758ad81dbb3173a4c9af251112c36f7662a554fb80be90c8f5788b1700000000'),
    merkle_root: hex('43b1368f320deb845f8e3527d93085516d01274f09815976db2d8a7145bff812'),
    height: 0,
    nonce: 3188021359,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1407444851,
    bits: 486604799, //Bits:0x1d00ffff
  },
  dnsSeeds: [ //TODO: ADD DNSSEEDS
    '54.94.161.254',
    '54.76.224.149'
  ],
  defaultClientPort: 41354
};




exports.mainnet = exports.livenet;

exports.testnet = {
  name: 'testnet',
  magic: hex('70352205'),
  addressVersion: 0x21,
  privKeyVersion: 161,
  P2SHVersion: 196,
  hkeyPublicVersion: 0x0488b21e,
  hkeyPrivateVersion: 0x0488ade4,
  genesisBlock: {
    hash: hex('758ad81dbb3173a4c9af251112c36f7662a554fb80be90c8f5788b1700000000'),
    merkle_root: hex('43b1368f320deb845f8e3527d93085516d01274f09815976db2d8a7145bff812'),
    height: 0,
    nonce: 3188021359,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1407444851,
    bits: 486604799, //Bits:0x1d00ffff
  },
  dnsSeeds: [ //TODO: ADD DNSSEEDS
    '54.94.161.254',
    '54.76.224.149'
  ],
  defaultClientPort: 41354
};
